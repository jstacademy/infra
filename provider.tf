terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.50.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "17.0.0"
    }
  }
}

provider "aws" {
  region = "eu-west-3"
}
