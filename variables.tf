variable "ami" {
  type        = string
  description = "Ubuntu AMI ID in eu-west-3 Region"
  default     = "ami-01d21b7be69801c2f"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t2.micro"
}

variable "name_tag" {
  type        = string
  description = "Name of the EC2 instance"
  default     = "MyInstance"
}
