resource "aws_instance" "my_vm" {
  ami           = var.ami
  instance_type = var.instance_type
  key_name      = "gitlab"

  tags = {
    Name = var.name_tag,
  }
}

resource "local_file" "ansible_inventory" {
  content = <<-EOF
    [ec2_instances]
      instance ansible_host=${aws_instance.my_vm.public_ip} ansible_user=ubuntu
        EOF

  filename = "${path.module}/inventory.ini"
}
